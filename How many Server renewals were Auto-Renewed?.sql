select auto_renewed,  count(distinct sen)
from public.license_renewal
where platform = 'Server'
and substring(cast(renewal_purchase_date as varchar),1,10) between '2015-07-01' and '2016-06-30'
and renewed = true
and existing_type = 'Renewal'
group by 1
;

select count(distinct sen)
from public.sale
where platform = 'Server'
and sale_type = 'Renewal'
and financial_year = 'FY2016'